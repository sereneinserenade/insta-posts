// @/store/index.js

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    isLiked: false,
  },
  mutations: {
    onLiked(state, isLiked) {
      state.isLiked = isLiked;
    }
  }
})
